package com.alectenharmsel.examples.spark

object Main {

  def main(args: Array[String]) {
    if (args.length < 1) {
      System.out.println("Usage: spark-examples <tool>")
      System.out.println("")
      System.out.println("Available tools are:")
      System.out.println("  * ngrams-length-by-year - Get average ngram length by year")
      System.out.println("  * reddit-j2p - transform reddit JSON to parquet")
      System.exit(1)
    }

    val remainingArgs = args.drop(1)
    args(0) match {
      case "ngrams-length-by-year" =>
        AverageNGramLength.main(remainingArgs)

      case "reddit-j2p" =>
        RedditJSONToParquet.main(remainingArgs)

      case "" =>
        System.exit(1)
    }
  }

}

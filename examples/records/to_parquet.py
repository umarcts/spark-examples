from pyspark import SQLContext, SparkConf, SparkContext
from pyspark.sql import Row
import sys

conf = SparkConf().setAppName("to_parquet")
sc = SparkContext(conf=conf)
sql = SQLContext(sc)

def toss_junk(raw):
    tmp = raw.split('|')
    if len(tmp) != 4:
        return False

    for thing in tmp:
        if len(thing) == 0:
            return False

    try:
        ident = int(tmp[0])
    except ValueError:
        return False

    return True


def to_row(raw):
    tmp = raw.split('|')

    return Row(
               id_number = int(tmp[0]),
               first_name = tmp[1],
               last_name = tmp[2],
               date = tmp[3]
              )


# schema is: id:int|fname:string|lname:string|date:string
raw = sc.textFile(sys.argv[1])
data_with_schema = raw.filter(toss_junk).map(to_row)
to_save = sql.createDataFrame(data_with_schema)
to_save.write.save(sys.argv[2], format="parquet")

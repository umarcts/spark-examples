package com.alectenharmsel.examples.spark

import java.io.IOException
import java.util.ArrayList
import java.util.List
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.scalatest._

class RedditJSONToParquetTest extends FlatSpec with Matchers {

  val raw = Context.sc.textFile(getClass.getResource("/RC_2007-10.small").toString)

  val rddOfRows = RedditJSONToParquet.rawToRows(raw)

  it should "have 22 lines" in {
    raw.count() should be (22)
  }

  it should "compute the same amount of rows as lines" in {
    raw.count() should be (rddOfRows.count())
  }

  // none of the sample comments are gilded
  it should "correctly parse out gilded" in {
    rddOfRows.first().getAs[Int]("gilded") should be (0)
  }

  it should "compute score to be 'ups - downs'" in {
    val r = rddOfRows.first()
    val downs = r.getAs[Int]("downs")
    val ups = r.getAs[Int]("ups")
    r.getAs[Int]("score") should be (ups - downs)
  }

}

/**
 * Operates on TSV with the following schema:
 *
 * 0. author (string)
 * 1. body (string)
 * 2. controversiality (int)
 * 3. downs (int)
 * 4. edited (bool)
 * 5. gilded (int)
 * 6. subreddit (string)
 * 7. ups (int)
 */

package com.alectenharmsel.examples.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import scala.collection.immutable.ListSet

object Reddit {

  private var in: String = null;

  def main(args: Array[String]) {
    if (args.length != 1) {
      System.err.println("Usage: Reddit <in>")
      System.exit(1)
    }

    in = args(0)

    val conf = new SparkConf().setAppName("Reddit " + in)
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    val sc = new SparkContext(conf)

    val raw = sc.textFile(in)

    val userSubs = getUserSubs(raw)
    userSubs.saveAsTextFile(in + "-userSubs")

    val pop = findPopularRelations(userSubs)
    val popNice = pop.map(tup => tup._2.toString + "," + tup._1.mkString(","))
    popNice.saveAsTextFile(in + "-pop")

    sc.stop()
  }

  def getUserSubs(raw: RDD[String]): RDD[(String, ListSet[String])] = {
    val reddit = raw.map(line => line.split("\t")).filter(arr => arr.size == 8)
    val userSubs = reddit.map(
      arr => (arr(0), new ListSet() + arr(6))
    ).reduceByKey(
      (a, b) => a ++ b
    )

    return userSubs
  }

  def findPopularRelations(in: RDD[(String, ListSet[String])]):
  RDD[(List[String], Int)] = {
    return in.map(
      tup => tup._2
    ).flatMap(
      set => set.toList.combinations(2)
    ).map(
      arr => (arr.sortWith(_.compareTo(_) < 0), 1)
    ).reduceByKey(
      (a, b) => a + b
    )
  }

}

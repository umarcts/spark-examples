package com.alectenharmsel.examples.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types._
import org.json4s._
import org.json4s.native.JsonMethods._
import scopt.OptionParser

object RedditJSONToParquet {

  implicit val formats = DefaultFormats

  val sparkSchema = StructType(
    StructField("archived", BooleanType, false) ::
    StructField("author", StringType, false) ::
    StructField("author_flair_css_class", StringType, true) ::
    StructField("author_flair_text", StringType, true) ::
    StructField("body", StringType, false) ::
    StructField("controversiality", IntegerType, false) ::
    StructField("created_utc", StringType, false) ::
    StructField("downs", IntegerType, false) ::
    StructField("edited", LongType, false) ::
    StructField("gilded", IntegerType, false) ::
    StructField("id", StringType, false) ::
    StructField("link_id", StringType, false) ::
    StructField("name", StringType, false) ::
    StructField("parent_id", StringType, false) ::
    StructField("retrieved_on", LongType, false) ::
    StructField("score", IntegerType, false) ::
    StructField("score_hidden", BooleanType, false) ::
    StructField("subreddit", StringType, false) ::
    StructField("subreddit_id", StringType, false) ::
    StructField("ups", IntegerType, false) ::
    Nil
  )

  case class Model(
    archived: Boolean,
    author: String,
    author_flair_css_class: String,
    author_flair_text: String,
    body: String,
    controversiality: Int,
    created_utc: String,
    downs: Int,
    edited: Option[Long],
    gilded: Int,
    id: String,
    link_id: String,
    name: String,
    parent_id: String,
    retrieved_on: Option[Long],
    score: Int,
    score_hidden: Boolean,
    subreddit: String,
    subreddit_id: String,
    ups: Int
  )

  case class Config(
    input: String = "",
    output: String = "",
    outputMode: String = "error"
  )

  var config: Config = Config()

  def jsonToRow(rawJson: String): Row = {
    val obj = parse(rawJson)
    val model = obj.extract[Model]

    var r = new GenericRowWithSchema(
      Array[Any](
        model.archived,
        model.author,
        model.author_flair_css_class,
        model.author_flair_text,
        model.body,
        model.controversiality,
        model.created_utc,
        model.downs,
        model.edited getOrElse 0.toLong,
        model.gilded,
        model.id,
        model.link_id,
        model.name,
        model.parent_id,
        model.retrieved_on getOrElse 0.toLong,
        model.score,
        model.score_hidden,
        model.subreddit,
        model.subreddit_id,
        model.ups
      ),
      sparkSchema
    )

    return r
  }

  def rawToRows(raw: RDD[String]): RDD[Row] = {
    raw.map(jsonToRow)
  }

  def parseArgs(args: Array[String]): Boolean = {
    val parser = new scopt.OptionParser[Config]("RedditJSONToParquet") {
      help("help") text("prints this usage text")

      opt[Unit]("append") action { (_, c) =>
        c.copy(outputMode = "append")
      } text("Save parquet files in append mode instead of erroring if the output exists")

      arg[String]("input") action { (x, c) =>
        c.copy(input = x)
      } text("An input file or directory")

      arg[String]("output") action { (x, c) =>
        c.copy(output = x)
      } text("An output directory")
    }

    parser.parse(args, Config()) match {
      case Some(c) =>
        config = c
        true

      case None =>
        false
    }
  }

  def main(args: Array[String]) {
    if (!parseArgs(args)) {
      System.exit(1)
    }

    val in = config.input
    val out = config.output

    val conf = new SparkConf().setAppName("Reddit JSON to Parquet")
    val sc = new SparkContext(conf)
    val sql = new SQLContext(sc)

    val json = sc.textFile(in)
    val parq = rawToRows(json)

    val df = sql.createDataFrame(parq, sparkSchema)

    df.write.mode(config.outputMode).format("parquet").save(out)
  }

}

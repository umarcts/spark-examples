# Example data

Upload the `WV_*.txt` files to your HDFS space:

    hdfs dfs -mkdir wv
    hdfs dfs -mkdir wv/birth-raw
    hdfs dfs -mkdir wv/death-raw
    hdfs dfs -put WV_births_1900.txt wv/birth-raw
    hdfs dfs -put WV_deaths_1960.txt wv/death-raw

Then, to convert the raw data to parquet, run:

    spark-submit --master yarn-client --queue <your_queue> to_parquet.py wv/birth-raw wv/birth-parquet
    spark-submit --master yarn-client --queue <your_queue> to_parquet.py wv/death-raw wv/death-parquet

In order to set up a library for jaro-winkler, run:

    module load anaconda3
    git clone git://github.com/jamesturk/jellyfish.git ~/jellyfish
    (cd ~/jellyfish && python setup.py bdist_egg)

Finally, to run the correlation, run:

    PYSPARK_PYTHON=$(which python) spark-submit \
            --master yarn-client \
            --queue <your_queue> \
            --num-executors 50 \
            --executor-memory 3g \
            --conf spark.executorEnv.PYTHONHASHSEED=0 \
            --py-files ~/jellyfish/dist/jellyfish-*.egg \
            correlate.py wv/birth-parquet wv/death-parquet wv/output
